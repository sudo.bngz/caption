package util

import (
	"fmt"
	"math/rand"
	"time"
)

var funnyWords = []string{
	"banana",
	"flamingo",
	"pickle",
	"whisker",
	"muffin",
	"cheeseburger",
	"penguin",
	"donut",
	"unicorn",
	"walrus",
	"noodle",
}

func GenerateRandomFilename() string {
	rand.Seed(time.Now().UnixNano())

	// Randomly select two funny words from the list
	word1 := funnyWords[rand.Intn(len(funnyWords))]
	word2 := funnyWords[rand.Intn(len(funnyWords))]

	// Generate a random number between 100 and 999
	number := rand.Intn(900) + 100

	// Construct the random filename
	filename := fmt.Sprintf("%s_%s_%d", word1, word2, number)

	return filename
}
