package util

import (
	"fmt"
	"strconv"
)

func HexToRGB(hex string) (int, int, int, error) {
	if len(hex) != 7 || hex[0] != '#' {
		return 0, 0, 0, fmt.Errorf("invalid hexadecimal color code: %s", hex)
	}

	r, err := strconv.ParseUint(hex[1:3], 16, 8)
	if err != nil {
		return 0, 0, 0, fmt.Errorf("invalid red component: %s", hex[1:3])
	}

	g, err := strconv.ParseUint(hex[3:5], 16, 8)
	if err != nil {
		return 0, 0, 0, fmt.Errorf("invalid green component: %s", hex[3:5])
	}

	b, err := strconv.ParseUint(hex[5:7], 16, 8)
	if err != nil {
		return 0, 0, 0, fmt.Errorf("invalid blue component: %s", hex[5:7])
	}

	return int(r), int(g), int(b), nil
}
