// This code is based on the open-source project "Meme" by nomad-software.
// Original code repository: https://github.com/nomad-software/meme

package image

import (
	"bytes"
	"caption/pkg/util"
	"fmt"
	"image"
	"image/draw"
	"image/gif"
	"image/jpeg"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/fatih/color"
	"github.com/fogleman/gg"
)

const (
	fontBorderRadius  = 3.0   // px
	fontLeading       = 1.4   // percentage
	maxFontSize       = 100.0 // pts
	topTextDivisor    = 5.0   // divisor
	bottomTextDivisor = 3.75  // divisor
	imageMargin       = 18.0  // px
	defaultFontPath   = "assets/fonts/impact.ttf"
)

// Image represents an image with meme-related functionalities.
type Image struct {
	ImagePath  string
	Width      float64
	Height     float64
	Type       string
	HexColor   string  // Caption text color
	FontPath   string  // Path to the font file
	FontSize   string  // Font size
	Position   string  // Text position
	MaxWidth   float64 // Maximum width of the caption text
	LineHeight float64 // Line height (spacing) between lines of the caption text
	Opacity    int     // Text opacity
	Quality    int     // Image quality
	Margin     int     // Text margin
	Stroke     int     // Stroke size
	*gg.Context
	*gif.GIF
}

// NewImage creates a new Image with the specified width and height.
func NewImage(width, height int) (*Image, error) {
	// Create a new context from gg
	ctx := gg.NewContext(width, height)

	// Initialize the Image struct with the context and default values
	img := &Image{
		ImagePath:  "",
		Width:      float64(width),
		Height:     float64(height),
		HexColor:   "#FFFFFF",
		FontPath:   defaultFontPath,
		FontSize:   "",
		Position:   "auto",
		MaxWidth:   150.0,
		LineHeight: 0.0,
		Opacity:    255,
		Quality:    100,
		Margin:     0,
		Stroke:     0,
		Context:    ctx,
	}

	return img, nil
}

// NewImageFromExisting creates a new Image from an existing image.
func Load(path string) (*Image, error) {
	_, err := os.Stat(path)
	if err != nil {
		return nil, fmt.Errorf("file not found: %s", err)
	}
	// Load the image
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("an error occurred while attempting to read the file: %s", err)
	}

	_, typ, err := image.DecodeConfig(bytes.NewReader(buf))
	if err != nil {
		return nil, fmt.Errorf("error reading image: %s", err)
	}

	var ctx *gg.Context
	var imgGif *gif.GIF

	if typ == "gif" {
		imgGif, err = gif.DecodeAll(bytes.NewReader(buf))
		if err != nil {
			return nil, fmt.Errorf("error reading image: %s", err)
		}
		firstFrame := imgGif.Image[0]
		ctx = gg.NewContextForImage(firstFrame)
	} else if typ == "jpeg" {
		img, _, err := image.Decode(bytes.NewReader(buf))
		if err != nil {
			return nil, fmt.Errorf("error reading image: %s", err)
		}
		ctx = gg.NewContextForImage(img)
	}

	// Initialize the Image struct with the context and default values
	imgNew := &Image{
		ImagePath:  path,
		Width:      float64(ctx.Width()),
		Height:     float64(ctx.Height()),
		Type:       typ,
		HexColor:   "#FFFFFF",
		FontPath:   defaultFontPath,
		FontSize:   "auto",
		Position:   "top",
		MaxWidth:   150.0,
		LineHeight: 1.5,
		Opacity:    255,
		Quality:    100,
		Margin:     20,
		Stroke:     5,
		Context:    ctx,
		GIF:        imgGif,
	}

	return imgNew, nil
}

// NewImageFromExisting creates a new Image from an existing image.
func LoadFromBuffer(buf []byte) (*Image, error) {
	img, err := jpeg.Decode(bytes.NewReader(buf))
	if err != nil {
		color.Red("Error reading image from buffer:", err)
		return nil, err
	}
	// Create a new context with the dimensions of the existing image
	ctx := gg.NewContextForImage(img)
	// Initialize the Image struct with the context
	newImg := &Image{
		Context:   ctx,
		ImagePath: "",
	}

	return newImg, nil
}

// Dynamically calculate the correct size needed for text.
func (img *Image) calculateFontSize(text string) float64 {
	for size := maxFontSize; size > 20; size-- {
		var rWidth, rHeight float64
		var lWidth, lHeight float64

		err := img.Context.LoadFontFace(img.FontPath, size)
		if err != nil {
			fmt.Println("Error during load font:", err)
		}

		lines := img.Context.WordWrap(text, img.Width)

		for _, line := range lines {
			lWidth, lHeight = img.Context.MeasureString(line)
			if lWidth > rWidth {
				rWidth = lWidth
			}
		}

		rHeight = (lHeight * fontLeading) * float64(len(lines))

		if rWidth <= img.Width && rHeight <= img.Height {
			return size
		}
	}
	return maxFontSize
}

type drawInfo struct {
	bounds image.Rectangle
	frame  *image.Paletted
	index  int
}

// Process drawing on each gif frame.
func (img *Image) processFrameDraw(dI drawInfo, text string, r, g, b int, fontSize, x, y, ax, ay float64, output chan drawInfo) {
	// Expand each frame, if needed, so it's the same size as the base.
	// This is to make it easier to draw and position the text.

	rgba := image.NewRGBA(dI.bounds)
	draw.Draw(rgba, dI.bounds, dI.frame, image.Point{}, draw.Src)

	ctx := gg.NewContextForImage(rgba)
	ctx.LoadFontFace(img.FontPath, fontSize)
	ctx.SetRGBA255(r, g, b, img.Opacity)
	ctx.DrawStringWrapped(text, x, y, ax, ay, float64(ctx.Width()), img.LineHeight, gg.AlignCenter)

	// Convert the graphic context to a paletted image.
	paletted := image.NewPaletted(dI.bounds, dI.frame.Palette)
	draw.Draw(paletted, dI.bounds, ctx.Image(), image.Point{}, draw.Src)

	dI.frame = paletted

	output <- dI
}

// DrawCaption draws a caption text on the Image at the specified position with the given options.
func (img *Image) DrawCaption(text string, x, y, ax, ay float64) {
	text = strings.ToUpper(text)
	// Calculate font size
	var fontSize float64
	if img.FontSize == "auto" {
		fontSize = img.calculateFontSize(text)
	} else {
		fontSize, _ = strconv.ParseFloat(img.FontSize, 64)
	}
	img.LoadFontFace(img.FontPath, fontSize)

	// Set the text color
	r, g, b, _ := util.HexToRGB(img.HexColor)

	if img.Type == "gif" {
		queue := make(chan drawInfo)
		for index, frame := range img.GIF.Image {
			fi := drawInfo{
				bounds: img.GIF.Image[0].Bounds(),
				frame:  frame,
				index:  index,
			}
			go img.processFrameDraw(fi, text, r, g, b, fontSize, x, y, ax, ay, queue)
		}

		for range img.GIF.Image {
			fi := <-queue
			img.GIF.Image[fi.index] = fi.frame
		}
		close(queue)
	} else {
		// Draw the text on the Image
		img.Context.DrawStringWrapped(text, x, y, ax, ay, img.Width, img.LineHeight, gg.AlignCenter)
	}
}

// Caption the image
func (img *Image) Caption(text string) error {
	switch img.Position {
	case "top":
		x := float64(img.Context.Width()) / 2
		y := float64(img.Margin)
		img.DrawCaption(text, x, y, 0.5, 0)
	case "center":
		x := img.Width / 2
		y := img.Height / 2
		img.DrawCaption(text, x, y, 0.5, 0.5)
	case "bottom":
		x := float64(img.Context.Width()) / 2
		y := float64(img.Context.Height() - img.Margin)
		img.DrawCaption(text, x, y, 0.5, 1.0)
	default:
		x := float64(img.Context.Width()) / 2
		y := float64(img.Margin)
		img.DrawCaption(text, x, y, 0.5, 0)
	}

	return nil
}

// AddCaptionSuffix adds a suffix to the file name in the given path and returns the updated path.
func (img *Image) addCaptionSuffix(filePath, suffix string) string {
	dir := filepath.Dir(filePath)
	ext := filepath.Ext(filePath)
	fileName := strings.TrimSuffix(filepath.Base(filePath), ext)
	newFileName := fmt.Sprintf("%s-%s%s", fileName, suffix, ext)
	newFilePath := filepath.Join(dir, newFileName)
	return newFilePath
}

// Save saves the Image as a JPG file with the given filename.
func (img *Image) Save() error {

	filename := img.ImagePath
	if filename == "" {
		filename = util.GenerateRandomFilename() + "." + img.Type
	}
	filename = img.addCaptionSuffix(filename, "caption")

	file, err := os.OpenFile(filepath.Base(filename), os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("failed to create file: %w", err)
	}
	defer file.Close()

	if img.Type == "gif" {

		var delays []int

		for range img.GIF.Image {
			delays = append(delays, 10)
		}

		err = gif.EncodeAll(file, &gif.GIF{
			Image: img.GIF.Image,
			Delay: delays,
		})
	} else {
		err = jpeg.Encode(file, img.Context.Image(), nil)
	}

	if err != nil {
		return fmt.Errorf("failed to save image: %w", err)
	}

	return nil
}

func (img *Image) StdOut() error {
	err := jpeg.Encode(os.Stdout, img.Context.Image(), nil)
	if err != nil {
		color.Red("Error encoding image:", err)
		return err
	}
	return nil
}
