package render

import (
	"caption/pkg/cli"
	meme "caption/pkg/image"
	"github.com/fatih/color"
	"io/ioutil"
	"os"
)

const (
	fontBorderRadius  = 3.0  // px
	fontLeading       = 1.4  // percentage
	maxFontSize       = 85.0 // pts
	topTextDivisor    = 5.0  // divisor
	bottomTextDivisor = 3.75 // divisor
	imageMargin       = 50.0 // px
)

func Render(opt cli.Options) error {

	var memePic *meme.Image
	var err error
	if opt.StdIn {
		// Read the image data from stdin
		imgData, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			color.Red("Error reading image data: %s", err)
			return err
		}
		// Load picture from stdin
		memePic, err = meme.LoadFromBuffer(imgData)
		if err != nil {
			color.Red("Error creating image: %s", err)
			return err
		}
	} else {
		// Load picture to caption
		memePic, err = meme.Load(opt.Image)
		if err != nil {
			color.Red("Error creating image: %s", err)
			return err
		}
	}
	// Set up the caption options
	captionOptions := &meme.CaptionOptions{
		HexColor:   opt.HexColor,
		FontPath:   "assets/fonts/Impact.ttf",
		FontSize:   opt.FontSize,
		MaxWidth:   float64(memePic.Context.Width()) * 0.8,
		LineHeight: 1.5,
		Opacity:    *opt.Opacity,
		Margin:     *opt.Margin,
		Stroke: 	*opt.Stroke,
	}

	// Draw text
	if opt.Position == "bottom" {
		memePic.BottomCaption(opt.Caption, captionOptions)
	} else if opt.Position == "center" {
		memePic.CenterCaption(opt.Caption, captionOptions)
	} else {
		memePic.TopCaption(opt.Caption, captionOptions)
	}

	// Save picture
	if opt.StdOut {
		memePic.StdOut()
	} else {
		memePic.Save()
	}
	return nil
}
