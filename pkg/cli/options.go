package cli

import (
	"flag"
	"fmt"
	"github.com/fatih/color"
	"regexp"
)

// Options represents the configuration options for the CLI program.
type Options struct {
	Image    string // Path to the image file.
	Caption  string // Text to caption the image.
	HexColor string // Color of the caption text in HTML color code format (e.g., "#FFFFFF" for white).
	FontSize string // Font size of the caption text.
	Position string // Position of the caption relative to the image (e.g., "top", "bottom").
	Opacity  *int   // Text opacity
	Margin   *int   // Margin size
	Stroke   *int   // Stroke size
	StdIn    bool   // Flag indicating to use stdin
	StdOut   bool   // Flag indicating to use stdout
	Help     bool   // Flag indicating whether to display the help information.
}

// ParseOptions parses the command line options.
func ParseOptions() Options {
	var opt Options

	flag.BoolVar(&opt.Help, "help", false, "Show help.\n")
	flag.StringVar(&opt.Image, "img", "", "Path to image file.\n")
	flag.StringVar(&opt.Caption, "caption", "", "The caption text.\n")
	flag.StringVar(&opt.FontSize, "fontsize", "auto", "Font size of the caption text.\n")
	flag.StringVar(&opt.HexColor, "color", "#FFFFFF", "text color as HTML color code\n.")
	flag.StringVar(&opt.Position, "position", "top", "Text position\n.")
	flag.BoolVar(&opt.StdIn, "stdin", false, "Use stdin.\n")
	flag.BoolVar(&opt.StdOut, "stdout", false, "Use stdout.\n")
	opt.Opacity = flag.Int("opacity", 255, "Text opacity 0-255\n.")
	opt.Margin = flag.Int("margin", 50, "Text position\n.")
	opt.Stroke = flag.Int("stroke", 5, "Stroke size\n.")

	flag.Parse()

	return opt
}

// validatePosition checks if the input string represents a valid position.
//
// A valid position is one of the following:
// - "top"
// - "center"
// - "bottom"
// - A string in the format "x:y", where "x" and "y" are non-negative integers.
//
// The function uses case-sensitive comparisons to check for "top", "center", and "bottom".
// In the "x:y" format, "x" and "y" must not contain leading zeroes.
//
// Function usage:
//
//	valid := validatePosition(pos)
//	if valid {
//	    fmt.Printf("%s is a valid position.\n", pos)
//	} else {
//	    fmt.Printf("%s is not a valid position.\n", pos)
//	}
//
// Args:
//
//	pos: A string representing the position.
//
// Returns:
//
//	A boolean value that's true if the input string is a valid position, and false otherwise.
func validPosition(pos string) bool {
	if pos == "top" || pos == "center" || pos == "bottom" {
		return true
	}

	// Check if input is in format 'x:y' where x and y are integers
	match, _ := regexp.MatchString(`^\d+:\d+$`, pos)
	return match
}

// Valid checks if the provided options are valid.
// It verifies that the required fields are provided and returns true if the options are valid, otherwise false.
// If any required field is missing, it prints an error message indicating the missing field.
//
// Parameters:
// - opt: Pointer to the Options struct containing the configuration options.
//
// Returns:
// - bool: true if the options are valid, false otherwise.
//
// Example usage:
//
//	options := Options{
//	  Image: "path/to/image.jpg",
//	}
//	isValid := options.Valid()
//	if !isValid {
//	  // Handle the invalid options
//	}
func (opt *Options) Valid() bool {

	if opt.Image == "" && !opt.StdIn {
		color.Red("An image is required")
		return false
	}

	if !validPosition(opt.Position) {
		color.Red("Incorrect position")
		return false
	}

	
	if *opt.Margin < 0 {
		return false
	}

	return true
}

// PrintUsage prints the usage information and examples for the CLI program.
// It displays a banner with the program name, followed by the usage information
// retrieved from the `flag.Usage()` function. Additionally, it provides example
// usages of the CLI program with a sample command and its description.
//
// Example usage:
//
//	options := Options{
//	  Image:   "kirk-khan",
//	  Caption: "|khaaaan",
//	}
//	options.PrintUsage()
//
// Output:
//
//		 ______     ______     ______   ______   __     ______     __   __
//	 /\  ___\   /\  __ \   /\  == \ /\__  _\ /\ \   /\  __ \   /\ "-.\ \
//	 \ \ \____  \ \  __ \  \ \  _-/ \/_/\ \/ \ \ \  \ \ \/\ \  \ \ \-.  \
//		 \ \_____\  \ \_\ \_\  \ \_\      \ \_\  \ \_\  \ \_____\  \ \_\\"\_\
//		  \/_____/   \/_/\/_/   \/_/       \/_/   \/_/   \/_____/   \/_/ \/_/
//
//
//			 Usage of meme:
//			   -image string
//			         Path to the image file.
//			   -caption string
//			         Text to caption the image.
//			   ...
//
//			 Examples:
//
//			   meme -i kirk-khan -t "|khaaaan"
func (opt *Options) PrintUsage() {
	var banner = `
	
 ______     ______     ______   ______   __     ______     __   __    
/\  ___\   /\  __ \   /\  == \ /\__  _\ /\ \   /\  __ \   /\ "-.\ \   
\ \ \____  \ \  __ \  \ \  _-/ \/_/\ \/ \ \ \  \ \ \/\ \  \ \ \-.  \  
 \ \_____\  \ \_\ \_\  \ \_\      \ \_\  \ \_\  \ \_____\  \ \_\\"\_\ 
  \/_____/   \/_/\/_/   \/_/       \/_/   \/_/   \/_____/   \/_/ \/_/ `

	color.Green(banner)
	fmt.Println("")
	flag.Usage()
	fmt.Println("")

	fmt.Println("  Example")
	fmt.Println("")
	color.Cyan("    caption -img grumpy.jpg -caption \"I read the manual once it was awful\"")
	fmt.Println("")
}
