# Go parameters
GO := go
BUILD_DIR := bin

# List of target architectures
ARCHITECTURES := linux/amd64 darwin/amd64 windows/amd64

# Build targets
.PHONY: all fetch-model (ARCHITECTURES) clean test build fmt

$(ARCHITECTURES):
	@echo "Building $@"
	@mkdir -p $(BUILD_DIR)/$@
	@$(GO) build -o $(BUILD_DIR)/$@/caption

# fetch xml models
fetch-model:
	@rm -fr assets/model
	@mkdir -p assets/model
	@curl -o assets/model/haarcascade_frontalface_default.xml https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_frontalface_default.xml

# Build the Go binary
build:
	@go build -o bin/caption ./main.go

# Clean the built binary
clean:
	@rm -rf bin/

# Clean the built binary
install:
	@cp bin/caption /usr/local/bin

# Run the application
run:
	@go run main.go

# Run the tests
test:
	@go test bin/caption -v

# Clean the built binary
fmt:
	@go fmt ./...

