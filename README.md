# Caption

A simple CLI tool written in pure Golang that generates memes.

## Requirements

- Go v1.16+

## Features

* Whip up memes straight from your own image files!
* No more guesswork - enjoy automatic text sizing
* Customize your memes with text opacity adjustments
* Add a pop of color - specify text color using HTML color codes
* Multi-platform magic - compatible with Linux, Mac, and Windows

## Installation

```bash
make build install
```

## Usage 

### Simple 

```bash
caption -img assets/tpl/everywhere.jpg -caption="bugs bugs everywhere"
```
Result:

<img src="doc/img/bugs-everywhere.jpg" width="500">

### Opacity and center

```bash
caption -img  assets/tpl/futurama-notsure.jpg  -caption "not sure if I can see the joke" -opacity 110 -fontsize 50 -position center
```
Result:

<img src="doc/img/notsure.jpg" width="500">

### Using pipeline

Using Unix pipelines, you can construct more complex memes:

```bash
caption -img=assets/tpl/grumpy-cat.jpg -caption="I tested my code once" -stdout | \
caption -stdin -caption "it was awful" -position bottom -color "#fc0328" -opacity 120 -stdout > doc/img/grumpy-dev-with-pipeline.jpg 
```

Result:

<img src="doc/img/grumpy-dev-with-pipeline.jpg" width="500">

## Help

Run the following command for help

```bash
caption -help                                                                                                                                                                                                                     
```