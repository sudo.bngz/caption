package main

import (
	"log"
	"os"

	meme "caption/pkg/image"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:    "image",
				Aliases: []string{"a"},
				Usage:   "Caption image",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "file",
						Value:    "",
						Usage:    "Image file",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "text",
						Value: "",
						Usage: "Text to caption",
					},
					&cli.StringFlag{
						Name:  "position",
						Value: "top",
						Usage: "Caption position",
					},
					&cli.StringFlag{
						Name:  "fontsize",
						Value: "auto",
						Usage: "Font size",
					},
					&cli.IntFlag{
						Name:  "opacity",
						Value: 255,
						Usage: "Opacity",
					},
					&cli.StringFlag{
						Name:  "color",
						Value: "#FFFFFF",
						Usage: "Color",
					},
					&cli.IntFlag{
						Name:  "stroke",
						Value: 0,
						Usage: "Stroke",
					},
					&cli.IntFlag{
						Name:  "margin",
						Value: 5,
						Usage: "Margin",
					},
				},
				Action: func(cCtx *cli.Context) error {
					file := cCtx.String("file")
					text := cCtx.String("text")
					position := cCtx.String("position")
					fontsize := cCtx.String("fontsize")
					opacity := cCtx.Int("opacity")
					color := cCtx.String("color")
					stroke := cCtx.Int("stroke")
					margin := cCtx.Int("margin")

					m, err := meme.Load(file)
					if err != nil {
						return err
					}

					m.Position = position
					m.FontSize = fontsize
					m.Opacity = opacity
					m.HexColor = color
					m.Stroke = stroke
					m.Margin = margin

					m.Caption(text)
					m.Save()

					return nil
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
